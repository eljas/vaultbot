package main

import (
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io"
	"io/ioutil"
	"math/big"
	"net"
	"os"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/vaultbot/auth"
	"gitlab.com/msvechla/vaultbot/cli"
)

var dirPath string
var certPath string
var caPath string
var privkeyPath string

func TestMain(m *testing.M) {
	// create test cert
	dirPath = setupTestFolder()
	certPath = fmt.Sprintf("%s/cert.pem", dirPath)
	caPath = fmt.Sprintf("%s/ca.pem", dirPath)
	privkeyPath = fmt.Sprintf("%s/key.pem", dirPath)

	// run tests
	code := m.Run()

	// clean up on test exit
	defer os.RemoveAll(certPath)
	os.Exit(code)
}

func TestSetupLogging(t *testing.T) {
	setupLogging(cli.Options{Logfile: fmt.Sprintf("%s/test.log", dirPath)})
}

func TestCheckAuthentication(t *testing.T) {
	opts := cli.Options{}

	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	client := createClient(opts)
	auth.CheckAuthentication(client)
}

func TestEndToEnd(t *testing.T) {
	opts := cli.Options{}

	opts.PKI.CertPath = fmt.Sprintf("%s/e2eCert.pem", dirPath)
	opts.PKI.CAChainPath = fmt.Sprintf("%s/e2eCA.pem", dirPath)
	opts.PKI.PrivKeyPath = fmt.Sprintf("%s/e2eKey.pem", dirPath)
	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	opts.PKI.CommonName = "vaultbot.test"
	opts.PKI.AltNames = "testing.com"
	opts.PKI.IPSans = "127.0.0.1,192.168.0.1"
	opts.PKI.Mount = "pki"
	opts.PKI.RoleName = "example-dot-com"
	opts.PKI.RenewPercent = 0.7

	// initial run test
	run(opts)

	cert := readCurrentCertificate(opts)
	if cert == nil {
		t.Fatal("Failed end to end test, no certificate found")
	}

	// check if file was not modified, because not yet expired
	oldHash := getFileHash(fmt.Sprintf("%s/e2eCert.pem", dirPath))

	// run again, should not modify
	run(opts)
	newHash := getFileHash(fmt.Sprintf("%s/e2eCert.pem", dirPath))
	if oldHash != newHash {
		t.Fatalf("Error in end to end test, certificate was modified when it was not expired!")
	}

	// run again, should modify now
	opts.PKI.RenewPercent = 0.000000001
	opts.PKI.PEMBundlePath = fmt.Sprintf("%s/e2eBundle.pem", dirPath)
	opts.RenewHook = fmt.Sprintf("touch %s/test.txt", dirPath)
	run(opts)
	renewHash := fmt.Sprintf("%s/e2eCert.pem", dirPath)
	if renewHash == newHash {
		t.Fatalf("Error in end to end test, certificate was not modified, although renew_percent should trigger!")
	}
}

func TestExecuteRenewHook(t *testing.T) {
	executeRenewHook(fmt.Sprintf("touch %s/test.txt", dirPath))

	if _, err := os.Stat(fmt.Sprintf("%s/test.txt", dirPath)); os.IsNotExist(err) {
		t.Fatal("Renew-hook test failed!")
	}
}

func TestRenewSelf(t *testing.T) {
	opts := cli.Options{}
	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myPeriodicToken"

	client := createClient(opts)
	renewSelf(client, opts)
}

func TestRequestCertificate(t *testing.T) {

	cn := "vaultbot.test"
	altNames := "vaultbot.test"
	ipSANS := "127.0.0.1,192.168.0.1"

	opts := cli.Options{}

	opts.PKI.CertPath = certPath
	opts.PKI.CAChainPath = caPath
	opts.PKI.PrivKeyPath = privkeyPath
	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	opts.PKI.CommonName = cn
	opts.PKI.AltNames = altNames
	opts.PKI.IPSans = ipSANS
	opts.PKI.Mount = "pki"
	opts.PKI.RoleName = "example-dot-com"

	client := createClient(opts)
	cert := requestCertificate(client, opts)
	if cert == nil {
		t.Fatal("Error requesting certificate")
	}

	writeCertificateData(cert, opts)
	certRead := readCurrentCertificate(opts)
	if certRead.Subject.CommonName != cn {
		t.Fatal("Received CN does not match requested CN!")
	}

}

func TestHasCertificateDataChanged(t *testing.T) {
	opts := cli.Options{}
	opts.PKI.CertPath = certPath
	createTestCert(certPath, "VaultBot.Test", []net.IP{net.ParseIP("127.0.0.1")}, []string{"testing.test", "helloworld.com", "VaultBot.Test"}, time.Now().Add(1*time.Hour))

	readCert := readCurrentCertificate(opts)

	// nothing changed, match created cert
	opts.PKI.CommonName = "VaultBot.Test"
	opts.PKI.IPSans = "127.0.0.1"
	opts.PKI.AltNames = "testing.test,helloworld.com"

	changed := hasCertificateDataChanged(readCert, opts)
	if changed {
		t.Fatal("Certificate data changed, when it shouldnt!")
	}

	// IP SANs len changed, does not match created cert
	opts.PKI.IPSans = "127.0.0.1,192.168.0.1"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("IP SANs len changed, when it should!")
	}

	// IP SANs fields changed, does not match created cert
	opts.PKI.IPSans = "192.168.0.1"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("IP SANs field changed, when it should!")
	}

	// dns alt names len changed, does not match created cert
	opts.PKI.AltNames = "helloworld.com"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Dns alt names len changed, when it should!")
	}

	// dns alt names fields changed, does not match created cert
	opts.PKI.AltNames = "helloworld.com,changed.com"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Dns alt names field changed, when it should!")
	}

	// common name changed, does not match created cert
	opts.PKI.AltNames = "testing.test,helloworld.com"
	opts.PKI.CommonName = "VaultBot.Changed"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Certificate data did not change, when it should!")
	}
}

func TestCertificateRenewalDue(t *testing.T) {
	opts := cli.Options{}
	opts.PKI.CertPath = certPath
	createTestCert(certPath, "VaultBot.Test", []net.IP{net.ParseIP("127.0.0.1")}, []string{"testing.test"}, time.Now().Add(1*time.Hour))

	// test certificate reading
	readCert := readCurrentCertificate(opts)

	if readCert == nil {
		t.Fatalf("Failed reading test certificate")
	}
	log.Println(readCert.Subject.CommonName)

	// test certificate expiry check
	// not yet due
	opts.PKI.RenewTime = "1m"
	if isCertificateRenewalDue(readCert, opts) {
		t.Fatalf("Certificate expiry check failed. Marked as due for renewal when it should not!")
	}

	// due
	opts.PKI.RenewTime = "2h"
	if !isCertificateRenewalDue(readCert, opts) {
		t.Fatalf("Certificate expiry check failed. Not marked as due for renewal when it should be!")
	}

	// forece renew
	opts.PKI.ForceRenew = true
	if !isCertificateRenewalDue(readCert, opts) {
		t.Fatalf("Certificate expiry check failed. Force renewal flag not working!")
	}
}

// creates a temp folder to store certificates
func setupTestFolder() string {
	dir, err := ioutil.TempDir("", "vaultbottest")
	if err != nil {
		log.Fatal(err)
	}

	return dir
}

// creates a test certificate with specified options
func createTestCert(path string, commonName string, ipSANs []net.IP, dnsSANs []string, notAfter time.Time) {
	RSAPrivateKey, _ := rsa.GenerateKey(rand.Reader, 1024)
	notBefore := time.Now()
	template := x509.Certificate{
		SerialNumber: new(big.Int).Lsh(big.NewInt(1), 128),
		Subject: pkix.Name{
			CommonName:   commonName,
			Organization: []string{"Acme Co"},
		},
		IsCA:                  true,
		IPAddresses:           ipSANs,
		DNSNames:              dnsSANs,
		NotBefore:             notBefore,
		NotAfter:              notAfter,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	certBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &RSAPrivateKey.PublicKey, RSAPrivateKey)
	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	certOut, err := os.Create(path)
	if err != nil {
		log.Fatalf("failed to open %s for writing: %s", path, err)
	}
	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: certBytes})
	certOut.Close()
}

func getFileHash(file string) string {
	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}
