package main

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/hashicorp/vault/api"
	"github.com/hashicorp/vault/helper/certutil"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/vaultbot/cli"
)

// requestCertificate returns the parsed certificate request response from vault
func requestCertificate(client *api.Client, options cli.Options) *certutil.ParsedCertBundle {

	log.Println("Requesting certificate...")

	rawCertData, err := client.Logical().Write(fmt.Sprintf("%s/issue/%s", options.PKI.Mount, options.PKI.RoleName), map[string]interface{}{
		"common_name":          options.PKI.CommonName,
		"alt_names":            options.PKI.AltNames,
		"ip_sans":              options.PKI.IPSans,
		"ttl":                  options.PKI.TTL,
		"exclude_cn_from_sans": options.PKI.ExcludeSans,
	})

	if err != nil {
		log.Fatalf("Error issues certificate request: %s", err.Error())
	}

	log.Println("Certificate data received.")

	certData, parseErr := certutil.ParsePKIMap(rawCertData.Data)
	if parseErr != nil {
		log.Fatalf("Error parsing certificate: %s", parseErr.Error())
	}

	return certData
}

// writeCertificateData persists a certificate bundle to the filesystem
func writeCertificateData(parsedCertBundle *certutil.ParsedCertBundle, options cli.Options) {

	// pem bundle
	var pemBundleOut *os.File
	var err error

	if options.PKI.PEMBundlePath != "" {
		pemBundleOut, err = os.Create(options.PKI.PEMBundlePath)
		if err != nil {
			log.Fatalf("Failed to open %s for writing: %s", options.PKI.PEMBundlePath, err)
		}
	}

	// private key
	keyOut, err := os.OpenFile(options.PKI.PrivKeyPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Failed to open %s for writing: %s", options.PKI.PrivKeyPath, err)
	}

	switch parsedCertBundle.PrivateKeyType {
	case certutil.RSAPrivateKey:
		pem.Encode(keyOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: parsedCertBundle.PrivateKeyBytes})
		if pemBundleOut != nil {
			pem.Encode(pemBundleOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: parsedCertBundle.PrivateKeyBytes})
		}
	case certutil.ECPrivateKey:
		pem.Encode(keyOut, &pem.Block{Type: "EC PRIVATE KEY", Bytes: parsedCertBundle.PrivateKeyBytes})
		if pemBundleOut != nil {
			pem.Encode(pemBundleOut, &pem.Block{Type: "EC PRIVATE KEY", Bytes: parsedCertBundle.PrivateKeyBytes})
		}
	}

	keyOut.Close()
	log.Printf("Wrote private key to: %s.", options.PKI.PrivKeyPath)

	// certificate
	certOut, err := os.Create(options.PKI.CertPath)
	if err != nil {
		log.Fatalf("Failed to open %s for writing: %s", options.PKI.CertPath, err)
	}

	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: parsedCertBundle.CertificateBytes})
	if pemBundleOut != nil {
		pem.Encode(pemBundleOut, &pem.Block{Type: "CERTIFICATE", Bytes: parsedCertBundle.CertificateBytes})
	}
	certOut.Close()
	log.Printf("Wrote certificate to: %s.", options.PKI.CertPath)

	// certificate chain
	chainOut, err := os.Create(options.PKI.CAChainPath)
	if err != nil {
		log.Fatalf("Failed to open %s for writing: %s", options.PKI.CAChainPath, err)
	}

	for _, cert := range parsedCertBundle.CAChain {
		pem.Encode(chainOut, &pem.Block{Type: "CERTIFICATE", Bytes: cert.Bytes})
		if pemBundleOut != nil {
			pem.Encode(pemBundleOut, &pem.Block{Type: "CERTIFICATE", Bytes: cert.Bytes})
		}
	}

	chainOut.Close()
	log.Printf("Wrote CA chain to: %s.", options.PKI.CAChainPath)

	if pemBundleOut != nil {
		pemBundleOut.Close()
		log.Printf("Wrote PEM Bundle to: %s.", options.PKI.PEMBundlePath)
	}
}

// readCurrentCertificate parses an existing certificate at the specified location
func readCurrentCertificate(options cli.Options) *x509.Certificate {
	if _, err := os.Stat(options.PKI.CertPath); err == nil {
		certFile, fileErr := ioutil.ReadFile(options.PKI.CertPath)
		if fileErr != nil {
			log.Fatalf("Unable to read certificate at %s: %s", options.PKI.CertPath, fileErr)
		}

		block, _ := pem.Decode([]byte(certFile))
		if block == nil {
			log.Fatalln("Failed to decode certificate PEM")
		}

		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			log.Fatalf("Failed to parse certificate at %s: %s", options.PKI.CertPath, err.Error())
		}
		return cert
	}
	return nil
}

// isCertificateRenewalDue checks a certificates expiry based on selected options
func isCertificateRenewalDue(cert *x509.Certificate, options cli.Options) bool {

	if options.PKI.ForceRenew {
		log.Println("Force renewal is activated. Skipping renewal due check...")
		return true
	}

	log.Println("Checking certificate expiry...")

	if cert != nil {
		// check if certificate is due for renewal
		if options.PKI.RenewTime != "" {
			renewDuration, timeErr := time.ParseDuration(options.PKI.RenewTime)
			if timeErr != nil {
				log.Fatalf("Unable to parse renew_duration %s: %s", options.PKI.RenewTime, timeErr.Error())
			}

			if time.Now().Add(renewDuration).After(cert.NotAfter) {
				log.Printf("Certificate due for renewal, expires %s", cert.NotAfter)
				return true
			}
			log.Printf("Certificate not yet due for renewal, will be renewed %s before expiry (%s)", renewDuration, cert.NotAfter)
		} else {
			// calculate percentage
			if (options.PKI.RenewPercent < 0.0) || (options.PKI.RenewPercent > 1.0) {
				log.Fatalf("Error: renew_percent must be a value between 0.0 and 1.0, got: %f", options.PKI.RenewPercent)
			}

			ttl := cert.NotAfter.Sub(cert.NotBefore)
			percSecs := ttl.Seconds() * options.PKI.RenewPercent
			duration, timeErr := time.ParseDuration(fmt.Sprintf("%fs", percSecs))
			if timeErr != nil {
				log.Fatalf("Unable to parse / calculate renew_percent %s: %s", options.PKI.RenewTime, timeErr.Error())
			}

			if time.Now().After(cert.NotBefore.Add(duration)) {
				log.Printf("Certificate due for renewal, expires %s", cert.NotAfter)
				return true
			}
			log.Printf("Certificate not yet due for renewal, will be renewed after: %s (%s after creation)", cert.NotBefore.Add(duration), duration)
		}

		return false
	}

	log.Printf("No certificate found at: %s. Skipping renewal due check...", options.PKI.CertPath)
	return true
}

// hasCertificateDataChanged verifies whether the requested data matches the data from an already existing certificate on the specified location.
// This ensures that no certificate is overwritten by mistake
func hasCertificateDataChanged(cert *x509.Certificate, options cli.Options) bool {
	// check common name
	if cert.Subject.CommonName != options.PKI.CommonName {
		log.Printf("Common name changed: old(%s) vs new(%s)", cert.Subject.CommonName, options.PKI.CommonName)
		return true
	}

	// check dns sans
	dnsNames := append(strings.Split(options.PKI.AltNames, ","), cert.Subject.CommonName)

	if len(cert.DNSNames) != len(dnsNames) {
		log.Printf("Dns alt names changed: old(%s) vs new(%s)", cert.DNSNames, dnsNames)
		return true
	}

	for _, sn := range cert.DNSNames {
		if !contains(dnsNames, sn) {
			log.Printf("Dns alt names changed: old(%s) vs new(%s)", cert.DNSNames, dnsNames)
			return true
		}
	}

	// check ip sans
	ipSans := strings.Split(options.PKI.IPSans, ",")

	if len(ipSans) != len(cert.IPAddresses) {
		log.Printf("IP SANs changed: old(%s) vs new(%s)", cert.IPAddresses, ipSans)
		return true
	}

	for _, ip := range cert.IPAddresses {
		if !contains(ipSans, ip.String()) {
			log.Printf("IP SANs changed: old(%s) vs new(%s)", cert.IPAddresses, ipSans)
			return true
		}
	}
	return false
}

// contains checks if array contains string
func contains(array []string, s string) bool {
	for _, i := range array {
		if i == s {
			return true
		}
	}
	return false
}
