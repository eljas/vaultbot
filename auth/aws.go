package auth

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/aws/aws-sdk-go/aws/ec2metadata"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/hashicorp/vault/api"
	"gitlab.com/msvechla/vaultbot/cli"
)

const (
	// MethodAWSIAM authenticates to vault via AWS IAM Auth
	MethodAWSIAM = "aws-iam"

	// MethodAWSEC2 authenticates to vault via AWS EC2 Auth
	MethodAWSEC2 = "aws-ec2"

	// AWSAuthErrorMessage is the default error message for AWS IAM Authentication
	AWSAuthErrorMessage = "Error authenticating to vault via AWS IAM authentication"

	// AWSAuthHeaderKey is the header key used during AWS IAM authentication
	AWSAuthHeaderKey = "X-Vault-AWS-IAM-Server-ID"
)

// AWSEC2 authenticates to vault via the AWS EC2 authentication method
func AWSEC2(options cli.Options, client *api.Client) {
	ec2m := ec2metadata.New(session.New())
	data, _ := ec2m.GetDynamicData("/instance-identity/pkcs7")

	loginData := make(map[string]interface{})
	loginData["pkcs7"] = data
	loginData["nonce"] = options.Vault.AWSAuthNonce
	loginData["role"] = options.Vault.AWSAuthRole

	// authenticate to vault
	resp, err := client.Logical().Write(fmt.Sprintf("auth/%s/login", options.Vault.AWSAuthMount), loginData)
	if err != nil {
		log.Fatalf("%s: %s", AWSAuthErrorMessage, err)
	}

	// retrieve the token
	var token string

	if token, err = resp.TokenID(); err != nil {
		log.Fatalf("Error retrieving token after authentication to vault via AWS EC2 authentication: %s", err)
	}

	client.SetToken(token)
}

// AWSIAM authenticates to vault via AWS IAM authentication
func AWSIAM(options cli.Options, client *api.Client) {
	// construct sts session
	stsSession := session.New()

	var params *sts.GetCallerIdentityInput
	svc := sts.New(stsSession)
	stsRequest, _ := svc.GetCallerIdentityRequest(params)

	// ADD VAULT AWS IAM auth header value
	if options.Vault.AWSAuthHeader != "" {
		stsRequest.HTTPRequest.Header.Add(AWSAuthHeaderKey, options.Vault.AWSAuthHeader)
	}
	stsRequest.Sign()

	// Extract values from request
	headersJSON, err := json.Marshal(stsRequest.HTTPRequest.Header)
	if err != nil {
		log.Fatalf("%s: %s", AWSAuthErrorMessage, err)
	}
	requestBody, err := ioutil.ReadAll(stsRequest.HTTPRequest.Body)
	if err != nil {
		log.Fatalf("%s: %s", AWSAuthErrorMessage, err)
	}

	loginData := make(map[string]interface{})
	loginData["iam_http_request_method"] = stsRequest.HTTPRequest.Method
	loginData["iam_request_url"] = base64.StdEncoding.EncodeToString([]byte(stsRequest.HTTPRequest.URL.String()))
	loginData["iam_request_headers"] = base64.StdEncoding.EncodeToString(headersJSON)
	loginData["iam_request_body"] = base64.StdEncoding.EncodeToString(requestBody)
	loginData["role"] = options.Vault.AWSAuthRole

	// authenticate to vault
	resp, err := client.Logical().Write(fmt.Sprintf("auth/%s/login", options.Vault.AWSAuthMount), loginData)
	if err != nil {
		log.Fatalf("%s: %s", AWSAuthErrorMessage, err)
	}

	// retrieve the token
	var token string

	if token, err = resp.TokenID(); err != nil {
		log.Fatalf("Error retrieving token after authentication to vault via AWS IAM authentication: %s", err)
	}

	client.SetToken(token)
}
