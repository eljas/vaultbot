package main

import (
	"os"

	goflags "github.com/jessevdk/go-flags"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/vaultbot/cli"
)

// VaultbotVersion bump before release
const VaultbotVersion = "1.1.0"

var options cli.Options

func main() {
	_, err := goflags.ParseArgs(&options, os.Args)

	if err != nil {
		os.Exit(1)
	}

	setupLogging(options)
	run(options)
}

// setupLogging configures logarus
func setupLogging(options cli.Options) {
	log.SetFormatter(&log.JSONFormatter{})
	file, err := os.OpenFile(options.Logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err == nil {
		log.SetOutput(file)
	} else {
		log.Printf("Failed to log to file, using default stderr: %s", err)
	}
}

// run executes all necessary methods in order to execute vaultbot based on specified options
func run(options cli.Options) {
	log.Printf("Running Vaultbot v%s", VaultbotVersion)
	client := createClient(options)
	if options.Vault.RenewToken {
		renewSelf(client, options)
	}

	var writeConfirmed = true
	var dueForRenewal = true

	currentCert := readCurrentCertificate(options)
	if currentCert != nil {
		dueForRenewal = isCertificateRenewalDue(currentCert, options)
		if dueForRenewal {
			if hasCertificateDataChanged(currentCert, options) {
				if !options.AutoConfirm {
					writeConfirmed = userConfirmation("Requested certificate data does not match existing certificate, continue anyways?")
				} else {
					log.Println("Requested certificate data does not match existing certificate, continuing anyways (auto confirmed)")
				}
			}
		}
	} else {
		log.Println("No existing certificate found, initial request.")
	}

	if writeConfirmed {
		if dueForRenewal {
			parsedCertBundle := requestCertificate(client, options)
			writeCertificateData(parsedCertBundle, options)
			if options.RenewHook != "" {
				executeRenewHook(options.RenewHook)
			}
			log.Println("Vaultbot finished successfully.")
		}
	} else {
		log.Println("Certificate renewal cancled by user.")
	}

}
